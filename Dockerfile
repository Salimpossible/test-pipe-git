FROM centos:centos8
RUN yum -y update
RUN yum -y install java-1.8.0-openjdk
RUN yum -y install git 
RUN yum -y install maven

CMD [export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.191.b12-1.el7_6.x86_64/jre/bin/java]
